unit raspy7_i2c;

{$mode objfpc}{$H+}

interface

uses
  Cthreads, Cmem, BaseUnix, Unix, Classes, SysUtils;


Const
  I2C_M_TEN          = $0010;  { we have a ten bit chip address }
  I2C_M_WR           = $0000;
  I2C_M_RD           = $0001;
  I2C_M_NOSTART      = $4000;
  I2C_M_REV_DIR_ADDR = $2000;
  I2C_M_IGNORE_NAK   = $1000;
  I2C_M_NO_RD_ACK    = $0800;

  I2C_RETRIES        = $0701;  { number of times a device address should be polled when not acknowledging       }
  I2C_TIMEOUT        = $0702;  { set timeout - call with int            }
  I2C_SLAVE          = $0703;  { Change slave address                   }
                               { Attn.: Slave address is 7 or 10 bits   }
  I2C_SLAVE_FORCE    = $0706;  { Change slave address
                                 Attn.: Slave address is 7 or 10 bits
                                 This changes the address, even if it
                                 is already taken!                      }
  I2C_TENBIT         = $0704;  { 0 for 7 bit addrs, != 0 for 10 bit     }

  I2C_FUNCS          = $0705;  { Get the adapter functionality          }
  I2C_RDWR           = $0707;  { Combined R/W transfer (one stop only)  }
  I2C_PEC            = $0708;  { != 0 for SMBus PEC                     }
  I2C_SMBUS          = $0720;  { SMBus-level access                     }

  I2C_CTRL_REG		 =  0; { Register Indexes }
  I2C_STATUS_REG	 =  1;
  I2C_DLEN_REG		 =  2;
  I2C_A_REG		 =  3;
  I2C_FIFO_REG		 =  4;
  I2C_DIV_REG		 =  5;
  I2C_DEL_REG		 =  6;
  I2C_CLKT_REG		 =  7;

  { To determine what functionality is present }
  I2C_FUNC_I2C                    = $00000001;
  I2C_FUNC_10BIT_ADDR             = $00000002;
  I2C_FUNC_PROTOCOL_MANGLING      = $00000004; { I2C_M_[REV_DIR_ADDR,NOSTART,..] }
  I2C_FUNC_SMBUS_PEC              = $00000008;
  I2C_FUNC_SMBUS_BLOCK_PROC_CALL  = $00008000; { SMBus 2.0 }
  I2C_FUNC_SMBUS_QUICK            = $00010000;
  I2C_FUNC_SMBUS_READ_BYTE        = $00020000;
  I2C_FUNC_SMBUS_WRITE_BYTE       = $00040000;
  I2C_FUNC_SMBUS_READ_BYTE_DATA   = $00080000;
  I2C_FUNC_SMBUS_WRITE_BYTE_DATA  = $00100000;
  I2C_FUNC_SMBUS_READ_WORD_DATA   = $00200000;
  I2C_FUNC_SMBUS_WRITE_WORD_DATA  = $00400000;
  I2C_FUNC_SMBUS_PROC_CALL        = $00800000;
  I2C_FUNC_SMBUS_READ_BLOCK_DATA  = $01000000;
  I2C_FUNC_SMBUS_WRITE_BLOCK_DATA = $02000000;
  I2C_FUNC_SMBUS_READ_I2C_BLOCK   = $04000000; { I2C-like block xfer  }
  I2C_FUNC_SMBUS_WRITE_I2C_BLOCK  = $08000000; { w/ 1-byte reg. addr. }
  I2C_FUNC_SMBUS_BYTE             = I2C_FUNC_SMBUS_READ_BYTE       or I2C_FUNC_SMBUS_WRITE_BYTE;
  I2C_FUNC_SMBUS_BYTE_DATA        = I2C_FUNC_SMBUS_READ_BYTE_DATA  or I2C_FUNC_SMBUS_WRITE_BYTE_DATA;
  I2C_FUNC_SMBUS_WORD_DATA        = I2C_FUNC_SMBUS_READ_WORD_DATA  or I2C_FUNC_SMBUS_WRITE_WORD_DATA;
  I2C_FUNC_SMBUS_BLOCK_DATA       = I2C_FUNC_SMBUS_READ_BLOCK_DATA or I2C_FUNC_SMBUS_WRITE_BLOCK_DATA;
  I2C_FUNC_SMBUS_I2C_BLOCK        = I2C_FUNC_SMBUS_READ_I2C_BLOCK  or I2C_FUNC_SMBUS_WRITE_I2C_BLOCK;


type

  { TR7I2c }

  TR7I2c = class
  private
    FAddress : byte;
    FBusNumber : byte;
    hdl : cint;
    procedure Close;
  public
    constructor Create;
    destructor Destroy; override;
    function Init(BusNumber, Address : byte) : boolean;
    function read_byte(addr : byte; var value : byte) : boolean;
    function read_byte_data(addr, cmd : byte; var value : byte) : boolean;
    function write_byte(cmd : byte) : boolean;
    function write_byte_data(addr, cmd , Value : byte) : boolean;
    function write_i2c_block_data(addr, cmd : byte; Block : pointer; size : integer) : boolean;
  end;

implementation

Type

  TByteBlock = packed array [0..$FF] of byte;
  PByteBlock = ^TByteBlock;


{ TR7I2c }

procedure TR7I2c.Close;
begin
  if hdl>0 then
    fpClose(hdl);
end;

constructor TR7I2c.Create;
begin
  inherited Create;
  hdl:=0;
end;

destructor TR7I2c.Destroy;
begin
  Close();
  inherited Destroy;
end;

function TR7I2c.Init(BusNumber, Address: byte): boolean;
begin
  Close;
  FBusNumber:=BusNumber;
  FAddress:=Address;
  hdl:=fpOpen('/dev/i2c-'+IntToStr(FBusNumber),O_RDWR);
  if hdl>=0 then
    Result:=fpIOctl(hdl, I2C_SLAVE, pointer(FAddress))>=0
  else
    Result:=false;
end;

function TR7I2c.write_byte_data(addr, cmd, Value: byte): boolean;
Var
  buff : packed array[0..1] of byte;
begin
  buff[0] := cmd;
  buff[1] := value;
  Result:=fpWrite(hdl,buff,2)=2;
end;

function TR7I2c.read_byte(addr: byte; var value: byte): boolean;
begin
  Result:=fpRead(hdl, Value, 1)=1
end;

function TR7I2c.read_byte_data(addr, cmd: byte; var value: byte): boolean;
begin
  if fpWrite(hdl,cmd,1)=1 then
    Result:=fpRead(hdl, Value, 1)=1
  else
    Result:=false;
end;

function TR7I2c.write_byte(cmd : byte): boolean;
begin
  Result:=fpWrite(hdl,cmd,1)=1
end;

function TR7I2c.write_i2c_block_data(addr, cmd : byte; Block : pointer; size : integer): boolean;
Var
  P : PByteBlock;
  B : Packed array[0..255] of byte;
  c : integer;
begin
  P:=PByteBlock(Block);
  if size>255 then size:=255;
  B[0]:=cmd;
  for c:=0 to Size-1 do
    B[c+1]:=P^[c];
  Result:=fpWrite(hdl, B, Size+1)=Size+1;
end;

end.

