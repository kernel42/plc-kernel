unit raspy7_lcd;

{$mode objfpc}{$H+}

interface

uses
  Cthreads, Cmem, BaseUnix, Unix, Classes, SysUtils,
  raspy7_utils, raspy7_i2c, raspy7_display;

Type

  TSPlit4 = packed array[1..4] of byte;

  { TR7HmiDisplay }

  TR7HmiDisplay = class(TR7Display)
  private
    porta          : byte;
    portb          : byte;
    ddrb           : byte;
    displayshift   : byte;
    displaymode    : byte;
    displaycontrol : byte;
    S1             : AnsiString;
    S2             : AnsiString;
    i2c            : TR7I2c;
    function Out4(bitmask, value : byte) : TSplit4;
    procedure writebyte(value : byte; const char_mode : boolean  = false);
    procedure writestring(text : Ansistring);
    procedure SetCursor(col, row : integer);
    procedure WriteMsg(x,y : integer; msg : AnsiString);
    procedure Home;
  protected
    function GetLine(index : integer): AnsiString; override;
    procedure SetLine(index : integer; AValue: AnsiString); override;
  public
    constructor Create;
    destructor Destroy; override;
    function Init(BusNumber, Address : byte) : boolean; override;
    function GetKeys : byte; override;
    procedure Clear; override;
  end;

implementation
Const
    // Port expander registers
    MCP23017_IOCON_BANK0    = $0A;  // IOCON when Bank 0 active
    MCP23017_IOCON_BANK1    = $15;  // IOCON when Bank 1 active
    // These are register addresses when in Bank 1 only:
    MCP23017_GPIOA          = $09;
    MCP23017_IODIRB         = $10;
    MCP23017_GPIOB          = $19;
    // Port expander input pin definitions
    SELECT                  = 0;
    RIGHT                   = 1;
    DOWN                    = 2;
    UP                      = 3;
    LEFT                    = 4;

    // LED colors
    OFF                     = $00;
    RED                     = $01;
    GREEN                   = $02;
    BLUE                    = $04;
    YELLOW                  = RED + GREEN;
    TEAL                    = GREEN + BLUE;
    VIOLET                  = RED + BLUE;
    WHITE                   = RED + GREEN + BLUE;
    LED_ON                  = RED + GREEN + BLUE;

    // LCD Commands
    LCD_CLEARDISPLAY        = $01;
    LCD_RETURNHOME          = $02;
    LCD_ENTRYMODESET        = $04;
    LCD_DISPLAYCONTROL      = $08;
    LCD_CURSORSHIFT         = $10;
    LCD_FUNCTIONSET         = $20;
    LCD_SETCGRAMADDR        = $40;
    LCD_SETDDRAMADDR        = $80;

    // Flags for display on/off control
    LCD_DISPLAYON           = $04;
    LCD_DISPLAYOFF          = $00;
    LCD_CURSORON            = $02;
    LCD_CURSOROFF           = $00;
    LCD_BLINKON             = $01;
    LCD_BLINKOFF            = $00;

    // Flags for display entry mode
    LCD_ENTRYRIGHT          = $00;
    LCD_ENTRYLEFT           = $02;
    LCD_ENTRYSHIFTINCREMENT = $01;
    LCD_ENTRYSHIFTDECREMENT = $00;

    // Flags for display/cursor shift
    LCD_DISPLAYMOVE = $08;
    LCD_CURSORMOVE  = $00;
    LCD_MOVERIGHT   = $04;
    LCD_MOVELEFT    = $00;

{ TR7HmiDisplay }

procedure TR7HmiDisplay.writebyte(value: byte; const char_mode : boolean  = false);
Var
  bitmask : byte;
  data : TSPlit4;
begin
  bitmask := portb AND $01; // Mask out PORTB LCD control bits
  if char_mode then bitmask:=bitmask OR $80; // Set data bit if not a command

  data:=Out4(bitmask, value);
  i2c.write_i2c_block_data(0,MCP23017_GPIOB,@data,4);
  portb:=data[4];

  if (not char_mode) and (value in[LCD_CLEARDISPLAY, LCD_RETURNHOME]) then
    sleep(100);
end;

procedure TR7HmiDisplay.writestring(text: Ansistring);
Var
  L, c, x, y : integer;
  bitmask : byte;
  data  : packed array[1..32] of byte;
  data4 : TSPlit4;
begin
  bitmask :=portb AND $01;  // Mask out PORTB LCD control bits
  bitmask :=bitmask OR $80; // Set data bit if not a command

  L:=Length(text);
  if L>16 then L:=16;
  y:=0;
  for c:=1 to L do
  begin
    data4:=Out4(bitmask,ord(text[c]));

    for x:=1 to 4 do
    begin
      inc(y);
      data[y]:=data4[x];
    end;

    if (y>=32) or (c=L) then
    begin
      i2c.write_i2c_block_data(0,MCP23017_GPIOB,@data,y);
      portb:=data[4];
      y:=0;
    end;

  end;
end;

procedure TR7HmiDisplay.SetCursor(col, row: integer);
Const
  row_offsets : array[0..3] of byte = ( $00, $40, $14, $54 );
Var
  x,y : byte;
begin

  if row>2 then row:=2;
  if row<1 then row:=1;

  if col>16 then col:=16;
  if col<1 then col:=1;

  y:=row-1;
  x:=col-1;

  writebyte(LCD_SETDDRAMADDR OR (x + row_offsets[y]));
end;

function TR7HmiDisplay.Out4(bitmask, value: byte): TSplit4;
Const
  flip : array[0..15] of byte = (
    $00, $10, $08, $18,
    $04, $14, $0C, $1C,
    $02, $12, $0A, $1A,
    $06, $16, $0E, $1E
  );
var
  hi, lo : byte;
begin
  hi := bitmask OR (flip[value SHR 4]);
  lo := bitmask OR (flip[value AND $0F]);
  Result[1]:=hi OR $20;
  Result[2]:=hi;
  Result[3]:=lo OR $20;
  Result[4]:=lo;
end;

constructor TR7HmiDisplay.Create;
begin
  inherited Create;
  i2c:=TR7I2c.Create;
  S1:=Blanks;
  S2:=Blanks;
end;

destructor TR7HmiDisplay.Destroy;
begin
  Clear;
  i2c.Free;
  inherited Destroy;
end;

function TR7HmiDisplay.Init(BusNumber, Address: byte): boolean;
Const
  banks : packed array[1..22] of byte = (
    $FF,  // IODIRA    R+G LEDs=outputs, buttons=inputs orig. 0b00111111
    $00,  // IODIRB    LCD D7=input, Blue LED=output
    $FF,  // IPOLA     Invert polarity on button inputs orig. 0b00111111
    $00,  // IPOLB
    $00,  // GPINTENA  Disable interrupt-on-change
    $00,  // GPINTENB
    $00,  // DEFVALA
    $00,  // DEFVALB
    $00,  // INTCONA
    $00,  // INTCONB
    $00,  // IOCON
    $00,  // IOCON
    $FF,  // GPPUA     Enable pull-ups on buttons orig. 0b00111111
    $00,  // GPPUB
    $00,  // INTFA
    $00,  // INTFB
    $00,  // INTCAPA
    $00,  // INTCAPB
    $00,  // GPIOA
    $00,  // GPIOB
    $00,  // OLATA     0 on all outputs; side effect of
    $00   // OLATB
  );
Var
  B : byte;
  c : integer;
begin
  Result:=i2c.Init(BusNumber, Address);
  if not Result then
    exit;
  porta := $00;
  portb := $00;
  ddrb  := $10;

  i2c.write_byte_data(0,MCP23017_IOCON_BANK1,$00);
  sleep(10);
  i2c.write_i2c_block_data(0,0,@banks,22);
  sleep(10);
  i2c.write_byte_data(0,MCP23017_IOCON_BANK0,$A0);
  sleep(10);

  displayshift   := (LCD_CURSORMOVE OR LCD_MOVERIGHT);
  displaymode    := (LCD_ENTRYLEFT  OR LCD_ENTRYSHIFTDECREMENT);
  displaycontrol := (LCD_DISPLAYON  OR LCD_CURSOROFF OR LCD_BLINKOFF);

  writebyte($33); // Init
  writebyte($32); // Init
  writebyte($28); // 2 line 5x8 matrix
  writebyte(LCD_CLEARDISPLAY);
  writebyte(LCD_CURSORSHIFT    OR displayshift);
  writebyte(LCD_ENTRYMODESET   OR displaymode);
  writebyte(LCD_DISPLAYCONTROL OR displaycontrol);
  writebyte(LCD_RETURNHOME);
end;

procedure TR7HmiDisplay.WriteMsg(x, y: integer; msg: AnsiString);
begin
  SetCursor(x,y);
  writestring(Msg);
end;

procedure TR7HmiDisplay.Clear;
begin
  writebyte(LCD_CLEARDISPLAY);
end;

procedure TR7HmiDisplay.Home;
begin
  writebyte(LCD_RETURNHOME);
end;

function TR7HmiDisplay.GetLine(index: integer): AnsiString;
begin
  case index of
    1 : Result:=S1;
    2 : Result:=S2;
  else
    Result:='';
  end;
end;

procedure TR7HmiDisplay.SetLine(index: integer; AValue: AnsiString);
begin
  AValue:=JustifyLeft(AValue,16,#32);
  case index of
    1 : if AValue<>S1 then
        begin
          S1:=AValue;
          WriteMsg(1,1,S1);
        end;
    2 : if AValue<>S2 then
        begin
          S2:=AValue;
          WriteMsg(1,2,S2);
        end;
  end;
end;

function TR7HmiDisplay.GetKeys: byte;
begin
  i2c.read_byte_data(0,MCP23017_GPIOA,Result);
end;

end.

