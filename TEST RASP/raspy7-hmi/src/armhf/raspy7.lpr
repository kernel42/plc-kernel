program raspy7;

{$mode objfpc}{$H+}

uses
  cthreads,
  BaseUnix,
  Classes,
  SysUtils,
  raspy7_core;

Const
  INFINITE = dword(-1);
Var
  HMI : TR7HmiManager;

procedure CloseProgram(reason : integer);
begin
  case reason of
    0 : Writeln('Terminating on request.');
    1 : Writeln('Terminating on signal.');
    2 : Writeln('HMI init failed.');
  end;
  HMI.Free;
  Halt;
end;

procedure DoExit(sig : cint);cdecl;
begin
  CloseProgram(1);
end;

begin
  HMI := TR7HmiManager.Create;
  if HMI.Init then
  begin
    fpSignal(SIGTERM,SignalHandler(@DoExit));
    sleep(INFINITE);
  end
  else
    CloseProgram(2);
end.

