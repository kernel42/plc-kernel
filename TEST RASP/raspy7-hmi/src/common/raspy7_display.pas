unit raspy7_display;
{$MODE DELPHI}
{$H+}

interface
uses
  Classes, SysUtils; 

Type

  { TR7Display }

  TR7Display = class
  protected
    function GetLine(index : integer): AnsiString; virtual;
    procedure SetLine(index : integer; AValue: AnsiString); virtual;
  public
    function Init(BusNumber, Address : byte) : boolean; virtual;
    function GetKeys : byte; virtual;
    procedure Clear; virtual;
    property Line[index : integer] : AnsiString read GetLine write SetLine; default;
  end;


implementation

{ TR7Display }

function TR7Display.GetLine(index: integer): AnsiString;
begin
  Result:='';
end;

procedure TR7Display.SetLine(index: integer; AValue: AnsiString);
begin
end;

function TR7Display.Init(BusNumber, Address : byte): boolean;
begin
  Result:=true;
end;

procedure TR7Display.Clear;
begin
end;

function TR7Display.GetKeys: byte;
begin
  Result:=0;
end;

end.

