unit raspy7_utils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

Const
  DisplayCols = 16;
  DisplayRows = 2;

procedure Elapse(var Elapsed : longword);
function DeltaTime(var Elapsed : longword) : longword;

function JustifyLeft(V : AnsiString; Len : integer; Pad : AnsiChar) : AnsiString;
function JustifyRight(V : AnsiString; Len : integer; Pad : AnsiChar) : AnsiString;
function AdjustRight(V : AnsiString; Len : integer) : AnsiString; overload;

function ByteToBin(B : Byte) : AnsiString;
function WordToBin(W : Word) : AnsiString;
function SwapWord(W : word) : word;
function SwapDWord(DW : cardinal) : cardinal;

function CenterText(S : AnsiString) : AnsiString;

function Chars(L : integer; Ch : AnsiChar) : Ansistring; overload;
function Chars(Ch : AnsiChar) : Ansistring; overload;
function Blanks(L : integer) : AnsiString; overload;
function Blanks : AnsiString; overload;

function TimValue(W : word) : AnsiString;

function Shrink(const S : string) : string;

procedure StrInsert(var S : AnsiString; Sub : AnsiString; X : integer);

function CharPos(S : String; C : Char) : integer;

implementation


function GetTickCount : longword;
begin
  Result := DWord(Trunc(Now * 24 * 60 * 60 * 1000));
end;

procedure Elapse(var Elapsed : longword);
begin
  Elapsed:=GetTickCount;
end;

function DeltaTime(var Elapsed : longword) : longword;
Var
  TheTime : longword;
begin
  TheTime:=GetTickCount;
  // Checks for rollover
  if TheTime<Elapsed then
    Elapsed:=0;
  Result:=TheTime-Elapsed;
end;

function JustifyLeft(V : AnsiString; Len : integer; Pad : AnsiChar) : AnsiString;
begin
  if Length(V)>Len then
  begin
    Result:=Copy(V,1,Len);
    exit;
  end;
  while Length(V)<Len do
    V:=V+Pad;
  Result:=V;
end;

function JustifyRight(V : AnsiString; Len : integer; Pad : AnsiChar) : AnsiString;
begin
  if Length(V)>Len then
  begin
    Result:=Copy(V,1,Len);
    exit;
  end;
  while Length(V)<Len do
    V:=Pad+V;
  Result:=V;
end;

function AdjustRight(V : AnsiString; Len : integer) : AnsiString; overload;
begin
  if Length(V)>Len then
    Result:=Chars(Len,'#')
  else
    Result:=JustifyRight(V,Len,#32);
end;

function ByteToBin(B : Byte) : AnsiString;
Const
  Mask : array[1..8] of byte = ($80,$40,$20,$10,$08,$04,$02,$01);
var
  c: Integer;
begin
  Result:='00000000';
  for c := 8 downto 1 do
    if (B and Mask[c])<>0 then
      Result[c]:='1';
end;

function WordToBin(W : Word) : AnsiString;
begin
  Result:=ByteToBin(W shr 8)+ByteToBin(W and $00FF);
end;

function SwapWord(W : word) : word;
Var
  IW : packed array[0..1] of byte absolute W;
  QW : packed array[0..1] of byte absolute Result;
begin
  QW[0]:=IW[1];
  QW[1]:=IW[0];
end;

function SwapDWord(DW : cardinal) : cardinal;
Var
  IW : packed array[0..3] of byte absolute DW;
  QW : packed array[0..3] of byte absolute Result;
begin
  QW[0]:=IW[3];
  QW[1]:=IW[2];
  QW[2]:=IW[1];
  QW[3]:=IW[0];
end;

function CenterText(S : AnsiString) : AnsiString;
begin
  if Length(S)<DisplayCols then
  begin
    while Length(S)<DisplayCols do
    begin
      if Length(S)<DisplayCols then
        S:=S+' ';
      if Length(S)<DisplayCols then
        S:=' '+S;
    end;
    Result:=S;
  end
  else
    Result:=Copy(S,1,DisplayCols);
end;

function Chars(L : integer; Ch : AnsiChar) : Ansistring; overload;
begin
  SetLength(Result,L);
  fillchar(Result[1],L,Ch);
end;

function Chars(Ch : AnsiChar) : Ansistring; overload;
begin
  Result:=Chars(DisplayCols, Ch);
end;

function Blanks(L : integer) : AnsiString; overload;
begin
  Result:=Chars(L,#32);
end;

function Blanks : AnsiString; overload;
begin
  Result:=Chars(#32);
end;

function TimValue(W : word) : AnsiString;
Var
  VT, Base : integer;
  VR : double;
begin
  VT := (W and $000F) +
  ((((W shr 4) and $000F)*10))+
  ((((W shr 8) and $000F)*100));
  Base:=(W shr 12) and $0003;
  case Base of
    0 : begin
          VR:=VT * 0.01;
          Str(VR:0:2,Result);
        end;
    1 : begin
          VR:=VT * 0.1;
          Str(VR:0:1,Result);
        end;
    2 : begin
          VR:=VT * 1.0;
          Str(VR:0:0,Result);
        end;
    3 : begin
          VR:=VT * 10.0;
          Str(VR:0:0,Result);
        end;
  end;
  // Normalize Length
  Result:=JustifyRight(Result,6,#32);
end;

function Shrink(const S : string) : string;
Var
  c : integer;
begin
  Result:='';
  for c:=1 to Length(S) do
    if S[c]<>' ' then
      Result:=Result+S[c];
end;

procedure StrInsert(var S : AnsiString; Sub : AnsiString; X : integer);
Var
  C, MaxLen : integer;
begin
  MaxLen:=DisplayCols-X+1;
  if Length(Sub)>MaxLen then
    Sub:=Chars(MaxLen,'#');
  for c:=1 to Length(Sub) do
    S[c+x-1]:=Sub[c];
end;

function CharPos(S : String; C : Char) : integer;
begin
  for Result:=1 to Length(S) do
    if S[Result]=C then
      exit;
  Result:=0;
end;

end.
