unit raspy7_core;
{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, XMLRead, DOM, IniFiles,
  snap7,
  raspy7_utils,
  {$IFDEF MSWINDOWS}
  raspy7_windisplay;
  {$ENDIF}
  {$IFDEF UNIX}
  raspy7_lcd;
  {$ENDIF}


Const
  ProgFileName  = 'r7program.xml';
  IniFileName   = 'raspy7.ini';

Type

  TNextPage = (npUp, npDn, npLeft, npRight);
  TFamilyKind = (fkUser, fkSys, fkVar);
  TKeyAction = (kaUp, kaDn, kaLeft, kaRight);

  TR7HmiManager = class;
  TR7HmiPageFamily = class;

  { TR7Plc }

  TR7Plc = class
  private
    FClient: TS7Client;
    FFullProtocol: boolean;
    FIP: string;
    FManager : TR7HmiManager;
    FLastError : integer;
    FConnected : boolean;
    FRack, FSlot : integer;
    function GetConnected: boolean;
    procedure SetFLastError(AValue: integer);
  public
    constructor Create(Manager : TR7HmiManager);
    destructor Destroy; override;
    procedure Disconnect;
    function Connect : boolean;
    function ReadArea(Area, DBNumber, Start, Amount, WordLen : integer; pUsrData : pointer) : boolean;
    function GetPlcStatus(Var Status : integer) : boolean;
    function GetPlcDateTime(Var DateTime : TDateTime) : boolean;
    function GetProtection(pUsrData : PS7Protection) : boolean;
    function ReadMultiVars(Items : PS7DataItems; ItemsCount : integer) : boolean;
    function Init(Node : TDOMNode) : boolean;
    property IP : string read FIP write FIP;
    property Connected : boolean read GetConnected;
    property LastError : integer read FLastError write SetFLastError;
    property Client : TS7Client read FClient write FClient;
    property FullProtocol : boolean read FFullProtocol;
  end;

  { TR7HmiPage }

  TR7HmiPage = class
  private
    FPageInfo : boolean;
    PICounter : longword; // Page Info timeout counter
  protected
    Error     : boolean;
    FActive   : boolean;
    FManager  : TR7HmiManager;
    FDisplay  : TR7HmiDisplay;
    FClient   : TS7Client;
    FPlc      : TR7Plc;
    Family    : TR7HmiPageFamily;
    procedure SetFActive(AValue: boolean); virtual;
    procedure ShowPageInfo; virtual;
    procedure SetFPageInfo(AValue: boolean); virtual;
    procedure ReadTags; virtual;
    procedure ShowTags; virtual;
  public
    GroupName : AnsiString;
    LevelName : AnsiString;
    PageUp    : TR7HmiPage;
    PageDn    : TR7HmiPage;
    PageLeft  : TR7HmiPage;
    PageRight : TR7HmiPage;
    constructor Create(Manager : TR7HmiManager);
    procedure SwitchPage(Where : TNextPage); virtual;
    procedure Refresh; virtual;
    property PageInfo : boolean read FPageInfo write SetFPageInfo;
    property Active : boolean read FActive write SetFActive;
  end;

  { TR7HmiSysPage }

  TR7HmiSysPage = class(TR7HmiPage)
  private
    procedure View0;
    procedure View1;
    procedure View2;
    procedure View3;
    procedure View4;
    procedure View5;
  protected
    View : integer;
    procedure ReadTags; override;
    procedure ShowTags; override;
    procedure ShowPageInfo; override;
  public
    constructor Create(Manager : TR7HmiManager);
    procedure SwitchPage(Where : TNextPage); override;
  end;

  { TR7HmiVarPage }

  TR7HmiVarPage = class(TR7HmiPage)
  protected
    Index  : integer;
    MinIdx : integer;
    MaxIdx : integer;
    Area   : integer;
    WordLen: byte;
  public
    constructor Create(Manager : TR7HmiManager);
    procedure SwitchPage(Where : TNextPage); override;
  end;

  { TR7HmiVarBitsPage }

  THmiVarBitKind = (sbkDigitalInputs, sbkDigitalOutputs, sbkMerkers);

  TR7HmiVarBitsPage = class(TR7HmiVarPage)
  private
    FKind  : THmiVarBitKind;
    Prefix : AnsiString;
    Data   : byte;
  protected
    procedure ShowPageInfo; override;
    procedure ReadTags; override;
    procedure ShowTags; override;
  public
    constructor Create(Manager : TR7HmiManager; Kind : THmiVarBitKind);
  end;

  { TR7HmiVarWordPage }

  THmiVarWordKind = (swkTimers, swkCounters);

  TR7HmiVarWordPage = class(TR7HmiVarPage)
  private
    Prefix : AnsiString;
    FKind  : THmiVarWordKind;
    Data   : array[0..1] of word;
  protected
    procedure ShowPageInfo; override;
    procedure ReadTags; override;
    procedure ShowTags; override;
  public
    constructor Create(Manager : TR7HmiManager; Kind : THmiVarWordKind);
  end;

  TR7HmiUsrPage = class;

  TR7TagType = (ttUnknown, ttBit, ttInt, ttUint, ttReal, ttChars, ttTxtIndex, ttConst);

  TR7TagInfo = record
    TagSize : integer;
    TagArea : integer;
    TagWLen : integer;
    TagDB   : integer;
    TagOfs  : integer;
    TagBit  : integer;
  end;

  { TR7HmiTag }

  TR7HmiTag = class
  protected
    X, Y  : integer;
    index : integer;
    FPage : TR7HmiUsrPage;
    TagVar: String;
    TagLen: integer;
    function GetTagVar(Node : TDOMNode) : boolean;
    function ValidateTagInfo(TI : TR7TagInfo) : boolean; virtual;
  public
    constructor Create(Page : TR7HmiUsrPage);
    procedure Show; virtual;
    function Init(Node : TDOMNode) : boolean; virtual;
  end;

  { TR7HmiBitTag }

  TR7HmiBitTag = class(TR7HmiTag)
  private
    ValTrue  : AnsiString;
    ValFalse : AnsiString;
    BitIndex : integer;
    Data     : packed array [0..3] of byte;
  protected
    function ValidateTagInfo(TI : TR7TagInfo) : boolean; override;
  public
    function Init(Node : TDOMNode) : boolean; override;
    procedure Show; override;
  end;

  { TR7HmiIntTag }

  TR7IntFormat = (ifDecimal, ifHex);
  TR7IntKind   = (ikShort, ikByte, ikSmall, ikWord, ikLong, ikDWord);

  TR7HmiIntTag = class(TR7HmiTag)
  protected
    Uint   : boolean;
    Format : TR7IntFormat;
    Kind   : TR7IntKind;
    Data   : longword;
    p_short: pshortint;
    p_byte : pbyte;
    p_sint : psmallint;
    p_word : pword;
    p_lint : plongint;
    p_lw   : plongword;
    function IntFormatOf(FormatName : string) : TR7IntFormat;
    function ValidateTagInfo(TI : TR7TagInfo) : boolean; override;
  public
    constructor Create(Page : TR7HmiUsrPage; Unsigned : boolean);
    function Init(Node : TDOMNode) : boolean; override;
    procedure Show; override;
  end;

  { TR7HmiTextIndexTag }

  TR7HmiTextIndexTag = class(TR7HmiIntTag)
  private
    Offset : integer;
  public
    constructor Create(Page : TR7HmiUsrPage);
    function Init(Node : TDOMNode) : boolean; override;
    procedure Show; override;
  end;

  { TR7HmiRealTag }

  TR7HmiRealTag = class(TR7HmiTag)
  private
    Data   : single;
    precision : integer;
    p_real : psingle;
    p_lw   : plongword;
  protected
    function ValidateTagInfo(TI : TR7TagInfo) : boolean; override;
  public
    constructor Create(Page : TR7HmiUsrPage);
    function Init(Node : TDOMNode) : boolean; override;
    procedure Show; override;
  end;

  { TR7HmiCharsTag }

  TR7HmiCharsTag = class(TR7HmiTag)
  private
    Data : packed array[1..64] of AnsiChar;
  protected
    function ValidateTagInfo(TI : TR7TagInfo) : boolean; override;
  public
    function Init(Node : TDOMNode) : boolean; override;
    procedure Show; override;
  end;

  { TR7HmiCstTag }

  TR7HmiCstTag = class(TR7HmiTag)
  private
    Value : AnsiString;
  public
    procedure Show; override;
    function Init(Node : TDOMNode) : boolean; override;
  end;

  { TR7HmiTags }

  TR7HmiTags = class(TList)
  private
    FPage : TR7HmiUsrPage;
  public
    constructor Create(Page : TR7HmiUsrPage);
    procedure Clear; override;
    procedure Show;
  end;

  { TR7HmiUsrPage }

  TR7HmiUsrPage = class(TR7HmiPage)
  private
    Tags       : TR7HmiTags;
    function TagTypeOf(TypeName : string) : TR7TagType;
  protected
    DataItems  : TS7DataItems;
    Buffer     : array[1..DisplayRows] of AnsiString;
    ActiveTags : integer;
    procedure ShowPageInfo; override;
    procedure ReadTags; override;
    procedure ShowTags; override;
  public
    constructor Create(Manager : TR7HmiManager);
    destructor Destroy; override;
    function Init(Node : TDOMNode; Level : integer) : boolean;
  end;


  { TR7HmiNullPage }

  TR7HmiNullPage = class(TR7HmiPage)
  protected
    procedure SwitchPage(Where : TNextPage); override;
  public
    procedure Refresh; override;
  end;

  { TR7HmiGroupList }

  TR7HmiGroupList = class(TList)
  private
    function GetGroup(index : integer): TR7HmiPage;
  public
    property Group[index : integer] : TR7HmiPage read GetGroup; default;
  end;

  { TR7HmiPageFamily }

  TR7HmiPageFamily = class(TList)
  private
    FActivePage: TR7HmiPage;
    function GetPage(index : integer): TR7HmiPage;
  public
    constructor Create;
    procedure Clear; override;
    property Pages[index : integer] : TR7HmiPage read GetPage; default;
    property ActivePage : TR7HmiPage read FActivePage write FActivePage;
  end;

  { TR7HmiKeyManager }

  TKeyStates = packed record
    OsUp, StUp : boolean; // One shoot and store up
    OsDn, StDn : boolean; // One shoot and store dn
    OsLf, StLf : boolean; // One shoot and store left
    OsRh, StRh : boolean; // One shoot and store right
    OsEn, StEn : boolean; // One shoot and store enter
    Menu : boolean;
  end;

  TR7HmiKeyManager = class
  private
    FManager : TR7HmiManager;
    FDisplay : TR7HmiDisplay;
    MnCounter: longword;
    UpCounter: longword;
    DnCounter: longword;
    // Internal States
    K        : TKeyStates;
    procedure ShowMenu;
  public
    constructor Create(Manager : TR7HmiManager);
    procedure Reset;
    function Refresh : boolean;
  end;

  { TR7HmiThread }

  TR7HmiThread = class(TThread)
  private
    FManager : TR7HmiManager;
  public
    constructor Create(Manager : TR7HmiManager);
    procedure Execute; override;
  end;

  { TR7HmiManager }

  TR7HmiProgramError = record
    Group : integer;
    Level : integer;
    TagNo : integer;
  end;

  TR7HmiManager = class
  private
    LastFileAge : longint;
    FActiveFamily: TFamilyKind;
    FDisplay: TR7HmiDisplay;
    FKeyManager: TR7HmiKeyManager;
    FMainThread : TR7HmiThread;
    FPlc: TR7Plc;
    FVarPages: TR7HmiPageFamily;
    FSysPages: TR7HmiPageFamily;
    FUsrPages: TR7HmiPageFamily;
    GroupList : TR7HmiGroupList;
    NullPage : TR7HmiNullPage;
    FText : TStringList;
    PrgCounter : longword;
    ProgramValid : boolean;
    GroupCount: integer;
    XMLDoc : TXMLDocument;
    function GetActivePage: TR7HmiPage;
    function CheckForNewProgram : boolean;
    function Connect : boolean;
    function CreateUsrPages : boolean;
    function LoadProgram : boolean;
    procedure Welcome;
    procedure CreateSysPages;
    procedure CreateVarPages;
    procedure CreateFixedPages;
    function Build : boolean;
    function NextGroup(Node : TDOMNode; GroupIndex : integer; var UpPage : TR7HmiPage) : boolean;
    function NextPage(Node : TDOMNode; Level : integer; GroupName : string; var UpPage, LeftPage : TR7HmiPage) : boolean;
    function InitTextList(Node : TDOMNode) : boolean;
    procedure LoadGlobals;
    procedure LinkGroups;
    procedure Clear;
    procedure Stop;
  public
    ProgramError : TR7HmiProgramError;
    Pages : array[TFamilyKind] of TR7HmiPageFamily;
    TcpError : boolean;
    ConnCnt : integer;
    HmiCnt : integer;
    constructor Create;
    destructor Destroy; override;
    function Init : boolean;   // First Init
    procedure ReInit; // Init on Program change
    procedure KeyAction(Action : TKeyAction);
    procedure Refresh;
    function GetIndexedText(index : integer) : AnsiString;
    property ActiveFamily : TFamilyKind read FActiveFamily write FActiveFamily;
    property ActivePage : TR7HmiPage read GetActivePage;
    property Display : TR7HmiDisplay read FDisplay write FDisplay;
    property KeyManager : TR7HmiKeyManager read FKeyManager write FKeyManager;
    property PLC : TR7Plc read FPlc write FPlc;
    property UsrPages : TR7HmiPageFamily read FUsrPages write FUsrPages;
    property SysPages : TR7HmiPageFamily read FSysPages write FSysPages;
    property VarPages : TR7HmiPageFamily read FVarPages write FVarPages;
  end;

implementation
Const
  TagReadError = ' **READ ERROR** ';

  TypeNames : array[TR7TagType] of string = (
    'UNKNOWN', 'BIT', 'INT', 'UINT', 'REAL', 'CHARS', 'TXTINDEX', 'CONST'
  );

  FormatNames : array[TR7IntFormat] of string = (
    'DECIMAL', 'HEX'
  );

Var
  I2cBusNumber     : longword = 1;
  MCP23017_Address : byte     = $20;
  NewPrgTimeout    : longword = 3000; // ms
  HmiRate          : longword = 100;  // ms
  PageInfoTime     : longword = 750;  // ms
  MenuInfoTime     : longword = 3000; // ms
  FastKeyTime      : longword = 1000; // ms
  WelcomeTime      : longword = 2000; // ms

function DOMGetProperty(Node : TDOMNode; Const PropName, DefaultValue : Ansistring) : string; overload;
Var
  PropNode  : TDOMNode;
begin
  if Assigned(Node) then
  begin
    PropNode:=Node.Attributes.GetNamedItem(PropName);
    if Assigned(PropNode) then
      Result:=AnsiString(PropNode.NodeValue)
    else
      Result:=DefaultValue;
  end
  else
    Result:='';
end;

function DOMGetProperty(Node : TDOMNode; Const PropName : Ansistring) : Ansistring; overload;
begin
  Result:=DOMGetProperty(Node, PropName, '');
end;

function DOMExistsProperty(Node : TDOMNode; Const PropName : string) : boolean;
Var
  PropNode  : TDOMNode;
begin
  if Assigned(Node) then
  begin
    PropNode:=Node.Attributes.GetNamedItem(PropName);
    Result:=Assigned(PropNode);
  end
  else
    Result:=false;
end;

function ParseDTag(TagVar : string; Var TagInfo : TR7TagInfo) : boolean;
Var
  p    : integer;
  S,S1 : string;
begin
  Result:=false;
  TagInfo.TagArea:=S7AreaDB;
  delete(TagVar,1,2); // skip DB
  // Get DB number
  p:=CharPos(TagVar,'.');
  if p=0 then
    exit;
  S1:=Copy(TagVar,1,p-1);
  Delete(TagVar,1,p);
  TagInfo.TagDB:=StrToIntDef(S1,0);
  if TagInfo.TagDB=0 then
    exit;
  // Get Var size
  S1:=Copy(TagVar,1,3);
  Delete(TagVar,1,3);
  if S1='DBB' then
  begin
    TagInfo.TagWLen:=S7WLByte;
    TagInfo.TagOfs:=StrToIntDef(TagVar,-1);
    if TagInfo.TagOfs=-1 then
      exit;
  end else
  if S1='DBW' then
  begin
    TagInfo.TagWLen:=S7WLWord;
    TagInfo.TagOfs:=StrToIntDef(TagVar,-1);
    if TagInfo.TagOfs=-1 then
      exit;
  end else
  if S1='DBD' then
  begin
    TagInfo.TagWLen:=S7WLDWord;
    TagInfo.TagOfs:=StrToIntDef(TagVar,-1);
    if TagInfo.TagOfs=-1 then
      exit;
  end else
  if S1='DBX' then
  begin
    TagInfo.TagWLen:=S7WLBit;
    // get Byte.Bit
    p:=CharPos(TagVar,'.');
    if p=0 then
      exit;
    S1:=Copy(TagVar,1,p-1);
    Delete(TagVar,1,p);
    TagInfo.TagOfs:=StrToIntDef(S1,-1);
    if TagInfo.TagOfs=-1 then // not a number
      exit;
    TagInfo.TagBit:=StrToIntDef(TagVar,-1);
    if TagInfo.TagBit=-1 then // not a number
      exit;
    if (TagInfo.TagBit<0) or (TagInfo.TagBit>7) then // invalid bit number
      exit;
  end
  else
    exit;

  Result:=true;
end;

function ParseStatic(TagVar : string; Var TagInfo : TR7TagInfo) : boolean;

  function GetNumber : boolean;
  Var
    c : integer;
  begin
    Delete(TagVar,1,1);
    Val(TagVar,TagInfo.TagOfs,c);
    Result:=c=0;
  end;

  function GetByteBit: boolean;
  Var
    p,c : integer;
    S : string;
  begin
    Result:=false;
    Delete(TagVar,1,1);
    p:=CharPos(TagVar,'.');
    if p=0 then
      exit;
    S:=Copy(TagVar,1,p-1);
    Val(S,TagInfo.TagOfs,c);
    if c=0 then
    begin
      Delete(TagVar,1,p);
      Val(TagVar,TagInfo.TagBit,c);
      Result:=(c=0) and (TagInfo.TagBit in [0..7]);
    end;
  end;

begin
  Result:=false;
  Delete(TagVar,1,1);
  if TagVar[1]='B' then
  begin
    TagInfo.TagWLen:=S7WLByte;
    Result:=GetNumber;
  end else
  if TagVar[1]='W' then
  begin
    TagInfo.TagWLen:=S7WLWord;
    Result:=GetNumber;
  end else
  if TagVar[1]='D' then
  begin
    TagInfo.TagWLen:=S7WLDWord;
    Result:=GetNumber;
  end
  else begin
    TagInfo.TagWLen:=S7WLBit;
    Result:=GetByteBit;
  end;
end;

function ParseETag(TagVar : string; Var TagInfo : TR7TagInfo) : boolean;
begin
  TagInfo.TagArea:=S7AreaPE;
  Result:=ParseStatic(TagVar, TagInfo);
end;

function ParseATag(TagVar : string; Var TagInfo : TR7TagInfo) : boolean;
begin
  TagInfo.TagArea:=S7AreaPA;
  Result:=ParseStatic(TagVar, TagInfo);
end;

function ParseMTag(TagVar : string; Var TagInfo : TR7TagInfo) : boolean;
begin
  TagInfo.TagArea:=S7AreaMK;
end;

function ParseTag(TagVar : string; Var TagInfo : TR7TagInfo) : boolean;
begin
  TagInfo.TagDB:=0;
  if Length(TagVar)>2 then
  begin
    if TagVar[1]='D' then Result:=ParseDTag(TagVar, TagInfo) else
      if TagVar[1]='E' then Result:=ParseETag(TagVar, TagInfo) else
        if TagVar[1]='A' then Result:=ParseATag(TagVar, TagInfo) else
          if TagVar[1]='M' then Result:=ParseMTag(TagVar, TagInfo) else
             Result:=false;
  end
  else
    Result:=false;
end;

{ TR7HmiBitTag }

function TR7HmiBitTag.ValidateTagInfo(TI: TR7TagInfo): boolean;
begin
  Result:=TI.TagWLen=S7WLBit;
end;

function TR7HmiBitTag.Init(Node: TDOMNode): boolean;
Var
  Visual : TDOMNode;
  TI     : TR7TagInfo;
begin
  Result:=inherited Init(Node) and GetTagVar(Node);
  if Result then
  begin
    Visual:=Node.FindNode('visual');
    if Assigned(Visual) then
    begin
      ValTrue :=DOMGetProperty(Visual,'true','1');
      ValFalse:=DOMGetProperty(Visual,'false','0');
    end;
    // Parse Tag
    Result:=ParseTag(TagVar, TI) and ValidateTagInfo(TI);
    if Result then
    begin
      inc(FPage.ActiveTags);
      index:=FPage.ActiveTags;
      FPage.DataItems[index-1].Area    :=TI.TagArea;
      FPage.DataItems[index-1].Amount  :=1;
      FPage.DataItems[index-1].DBNumber:=TI.TagDB;
      FPage.DataItems[index-1].Start   :=TI.TagOfs;
      FPage.DataItems[index-1].WordLen :=S7WLByte;
      FPage.DataItems[index-1].pdata   :=@Data;
      BitIndex:=TI.TagBit;
    end;
  end;
end;

procedure TR7HmiBitTag.Show;
Const
  Mask : array[0..7] of byte = ($80,$40,$20,$10,$08,$04,$02,$01);
begin
  if (Data[0] and Mask[BitIndex])<>0 then
    StrInsert(FPage.Buffer[Y],ValTrue,X)
  else
    StrInsert(FPage.Buffer[Y],ValFalse,X)
end;

{ TR7HmiTextIndexTag }

constructor TR7HmiTextIndexTag.Create(Page: TR7HmiUsrPage);
begin
  inherited Create(Page, false);
  Offset:=0;
end;

function TR7HmiTextIndexTag.Init(Node: TDOMNode): boolean;
Var
  Visual : TDOMNode;
  Mask   : string;
  NoMask : boolean;
  TI     : TR7TagInfo;
begin
  Result:=inherited Init(Node);
  if Result then
  begin
    TagLen:=DisplayCols-x+1;
    Visual:=Node.FindNode('visual');
    if Assigned(Visual) then
    begin
      TagLen:=StrToIntDef(DOMGetProperty(Visual,'len'),TagLen);
      Offset:=StrToIntDef(DOMGetProperty(Visual,'offset'),Offset);
    end;
  end;
end;

procedure TR7HmiTextIndexTag.Show;
Var
  iValue : int64;
  Value  : AnsiString;
begin
  // Adjust HI-LO order
  case Kind of
    ikSmall,
    ikWord  : p_word^:=SwapWord(p_word^);
    ikLong,
    ikDWord : p_lw^:=SwapDWord(p_lw^);
  end;

  case Kind of
    ikShort : iValue:=p_short^;
    ikByte  : iValue:=p_byte^;
    ikSmall : iValue:=p_sint^;
    ikWord  : iValue:=p_word^;
    ikLong  : iValue:=p_lint^;
    ikDWord : iValue:=p_lw^;
  end;

  Value:=FPage.FManager.GetIndexedText(iValue+Offset);
  if Value='' then
    Value:=Blanks(TagLen)
  else
    Value:=JustifyLeft(Value,TagLen,#32);

  StrInsert(FPage.Buffer[Y],Value,X);
end;

{ TR7HmiCharsTag }

function TR7HmiCharsTag.ValidateTagInfo(TI: TR7TagInfo): boolean;
begin
  Result:=TI.TagWLen = S7WLByte;
end;

function TR7HmiCharsTag.Init(Node: TDOMNode): boolean;
Var
  Visual : TDOMNode;
  TI     : TR7TagInfo;
begin
  Result:=inherited Init(Node) and GetTagVar(Node);
  if Result then
  begin
    Result:=false;
    Visual:=Node.FindNode('visual');
    if Assigned(Visual) then
    begin
      TagLen:=StrToIntDef(DOMGetProperty(Visual,'len'),0);
      if (TagLen < 1) or (TagLen > DisplayCols) then
        exit;
    end
    else
      exit;
    // Parse Tag
    Result:=ParseTag(TagVar, TI) and ValidateTagInfo(TI);
    if Result then
    begin
      inc(FPage.ActiveTags);
      index:=FPage.ActiveTags;
      FPage.DataItems[index-1].Area    :=TI.TagArea;
      FPage.DataItems[index-1].Amount  :=TagLen;
      FPage.DataItems[index-1].DBNumber:=TI.TagDB;
      FPage.DataItems[index-1].Start   :=TI.TagOfs;
      FPage.DataItems[index-1].WordLen :=S7WLByte;
      FPage.DataItems[index-1].pdata   :=@Data;
    end;
  end;
end;

procedure TR7HmiCharsTag.Show;
Var
  Value : AnsiString;
  c : integer;
begin
  Value:='';
  c:=1;
  while (byte(Data[c])<>0) and (byte(Data[c])<>10) and (byte(Data[c])<>13) and (c<=TagLen) do
  begin
    Value:=Value+Data[c];
    inc(c);
  end;
  StrInsert(FPage.Buffer[Y],Value,X);
end;

{ TR7HmiRealTag }

function TR7HmiRealTag.ValidateTagInfo(TI: TR7TagInfo): boolean;
begin
  Result:=TI.TagWLen = S7WLDWord;
end;

constructor TR7HmiRealTag.Create(Page: TR7HmiUsrPage);
begin
  inherited Create(Page);
  p_real :=@Data;     // Data as S7 Real (float/single) buffer
  p_lw   :=@Data;     // Data as DWord (uint32) buffer
end;

function TR7HmiRealTag.Init(Node: TDOMNode): boolean;
Var
  Visual : TDOMNode;
  Mask   : string;
  p      : integer;
  TI     : TR7TagInfo;
begin
  Result:=inherited Init(Node) and GetTagVar(Node);
  if Result then
  begin
    Result:=false;
    Visual:=Node.FindNode('visual');
    if Assigned(Visual) then
    begin
      Mask:=DOMGetProperty(Visual,'mask');
      // Length
      if Mask<>'' then
      begin
        TagLen:=Length(Mask);
        p:=CharPos(Mask,'.');
        if p=0 then
          Precision:=0
        else
          Precision:=TagLen-p;
      end
      else
        exit;
    end
    else
      exit;
    // Parse Tag
    Result:=ParseTag(TagVar, TI) and ValidateTagInfo(TI);
    if Result then
    begin
      inc(FPage.ActiveTags);
      index:=FPage.ActiveTags;
      FPage.DataItems[index-1].Area    :=TI.TagArea;
      FPage.DataItems[index-1].Amount  :=1;
      FPage.DataItems[index-1].DBNumber:=TI.TagDB;
      FPage.DataItems[index-1].Start   :=TI.TagOfs;
      FPage.DataItems[index-1].WordLen :=S7WLReal;
      FPage.DataItems[index-1].pdata   :=@Data;
    end;
  end;
end;

procedure TR7HmiRealTag.Show;
Var
  Value : string;
begin
  // Adjust HI-LO order
  p_lw^:=SwapDWord(p_lw^);
  Str(p_real^:TagLen:Precision,Value);
  Value:=AdjustRight(Value,TagLen);
  StrInsert(FPage.Buffer[Y],Value,X);
end;

{ TR7HmiIntTag }

function TR7HmiIntTag.IntFormatOf(FormatName: string): TR7IntFormat;
begin
  for Result:=ifDecimal to ifHex do
    if UpperCase(FormatName)=FormatNames[Result] then
      exit;
  Result:=ifDecimal;
end;

function TR7HmiIntTag.ValidateTagInfo(TI: TR7TagInfo): boolean;
begin
  Result:=TI.TagWLen in [S7WLByte, S7WLWord, S7WLDWord];
end;

constructor TR7HmiIntTag.Create(Page: TR7HmiUsrPage; Unsigned : boolean);
begin
  inherited Create(Page);
  UInt := Unsigned;
  Format:=ifDecimal;  // Default
  p_short:=@Data;     // Data as ShortInt (int8) buffer
  p_byte :=@Data;     // Data as Byte (uint8) buffer
  p_sint :=@Data;     // Data as Smallint (int16) buffer
  p_word :=@Data;     // Data as Word (uint16) buffer
  p_lint :=@Data;     // Data as LongInt (int32) buffer
  p_lw   :=@Data;     // Data as DWord (uint32)buffer
end;

function TR7HmiIntTag.Init(Node: TDOMNode): boolean;
Var
  Visual : TDOMNode;
  Mask   : string;
  NoMask : boolean;
  TI     : TR7TagInfo;
begin
  Result:=inherited Init(Node) and GetTagVar(Node);
  if Result then
  begin
    Visual:=Node.FindNode('visual');
    if Assigned(Visual) then
    begin
      Format:=IntFormatOf(DOMGetProperty(Visual,'format','decimal'));
      Mask:=DOMGetProperty(Visual,'mask');
      // Length
      if Mask<>'' then
      begin
        NoMask:=false;
        TagLen:=Length(Mask);
      end
      else
        NoMask:=true;
    end;

    // Parse Tag
    Result:=ParseTag(TagVar, TI) and ValidateTagInfo(TI);
    if Result then
    begin
      inc(FPage.ActiveTags);
      index:=FPage.ActiveTags;
      FPage.DataItems[index-1].Area    :=TI.TagArea;
      FPage.DataItems[index-1].Amount  :=1;
      FPage.DataItems[index-1].DBNumber:=TI.TagDB;
      FPage.DataItems[index-1].Start   :=TI.TagOfs;
      FPage.DataItems[index-1].WordLen :=TI.TagWLen;
      FPage.DataItems[index-1].pdata   :=@Data;
      // Integer Kind
      case TI.TagWLen of
        S7WLByte  : if UInt then Kind:=ikByte else Kind:=ikShort;
        S7WLWord  : if UInt then Kind:=ikWord else Kind:=ikSmall;
        S7WLDWord : if UInt then Kind:=ikDWord else Kind:=ikLong;
      end;
      // Length if no mask was found
      if NoMask then
      begin
        case Kind of
          ikShort : TagLen:=4;
          ikByte  : TagLen:=3;
          ikSmall : TagLen:=6;
          ikWord  : TagLen:=5;
          ikLong  : TagLen:=11;
          ikDWord : TagLen:=10;
        end;
      end;
    end;
  end;
end;

procedure TR7HmiIntTag.Show;
Var
  Value  : string;

  function AsDecimal : string;
  Var
    Value : AnsiString;
  begin
    case Kind of
      ikShort : Str(p_short^,Value);
      ikByte  : Str(p_byte^,Value);
      ikSmall : Str(p_sint^,Value);
      ikWord  : Str(p_word^,Value);
      ikLong  : Str(p_lint^,Value);
      ikDWord : Str(p_lw^,Value);
    end;

    if Length(Value)>TagLen then
      Result:=Chars(TagLen,'#')
    else
      Result:=AdjustRight(Value,TagLen);
  end;

  function AsHex : string;
  Var
    Value : AnsiString;
  begin
    case Kind of
      ikShort : Value:=IntToHex(p_short^,TagLen);
      ikByte  : Value:=IntToHex(p_byte^,TagLen);
      ikSmall : Value:=IntToHex(p_sint^,TagLen);
      ikWord  : Value:=IntToHex(p_word^,TagLen);
      ikLong  : Value:=IntToHex(p_lint^,TagLen);
      ikDWord : Value:=IntToHex(p_lw^,TagLen);
    end;

    if Length(Value)>TagLen then
      Result:=Chars(TagLen,'#')
    else
      Result:=AdjustRight(Value,TagLen);
  end;

begin
  // Adjust HI-LO order
  case Kind of
    ikSmall,
    ikWord  : p_word^:=SwapWord(p_word^);
    ikLong,
    ikDWord : p_lw^:=SwapDWord(p_lw^);
  end;

  // Build StrValue
  Case Format of
    ifDecimal : Value:=AsDecimal;
    ifHex     : Value:=AsHex;
  end;

  StrInsert(FPage.Buffer[Y],Value,X);
end;

{ TR7HmiCstTag }

procedure TR7HmiCstTag.Show;
begin
  StrInsert(FPage.Buffer[Y],Value,X);
end;

function TR7HmiCstTag.Init(Node: TDOMNode): boolean;
Var
  MaxLen : integer;
begin
  Result:=inherited Init(Node);
  if Result then
    Value:=DOMGetProperty(Node,'value');
  MaxLen:=DisplayCols-X+1;
  if Length(Value)>MaxLen then
    Value:=Copy(Value,1,MaxLen);
end;

function TR7HmiTag.GetTagVar(Node : TDOMNode): boolean;
begin
  TagVar:=Shrink(UpperCase(DOMGetProperty(Node,'var')));
  Result:=TagVar<>'';
end;

function TR7HmiTag.ValidateTagInfo(TI: TR7TagInfo): boolean;
begin
  Result:=true;
end;

constructor TR7HmiTag.Create(Page: TR7HmiUsrPage);
begin
  inherited Create;
  FPage:=Page;
end;

procedure TR7HmiTag.Show;
begin
end;

function TR7HmiTag.Init(Node: TDOMNode): boolean;
Var
  Visual : TDOMNode;
begin
  Visual:=Node.FindNode('visual');
  if Assigned(Visual) then
  begin
    X:=StrToIntDef(DOMGetProperty(Visual,'x'),0);
    Y:=StrToIntDef(DOMGetProperty(Visual,'y'),0);
    Result:=(X in [1..DisplayCols]) and (Y in [1..DisplayRows]);
  end
  else
    Result:=false;
end;

{ TR7HmiTags }

constructor TR7HmiTags.Create(Page : TR7HmiUsrPage);
begin
  inherited Create;
  FPage:=Page;
end;

procedure TR7HmiTags.Clear;
Var
  c, max : integer;
  aTag : TR7HmiTag;
begin
  max:=count-1;
  for c:=0 to max do
  begin
    aTag:=TR7HmiTag(Items[c]);
    aTag.Free;
  end;
  inherited Clear;
end;

procedure TR7HmiTags.Show;
Var
  c : integer;
begin
  for c:=1 to DisplayRows do
    FPage.Buffer[c]:=Blanks;

  for c:=0 to count-1 do
    TR7HmiTag(Items[c]).Show;
end;

{ TR7HmiNullPage }

procedure TR7HmiNullPage.SwitchPage(Where: TNextPage);
begin
end;

procedure TR7HmiNullPage.Refresh;
begin
  FDisplay.Line[1]:=CenterText('NO GROUPS FOUND');
  FDisplay.Line[2]:=CenterText('GOTO SYS OR VAR');
end;

{ TR7Plc }

function TR7Plc.GetConnected: boolean;
begin

end;

procedure TR7Plc.SetFLastError(AValue: integer);
begin
  FLastError:=AValue;
  FManager.TcpError:=(FLastError and $0000FFFF)<>0;
end;

constructor TR7Plc.Create(Manager: TR7HmiManager);
begin
  inherited Create;
  FManager := Manager;
  FClient := TS7Client.Create;
  FConnected:=false;
  FRack:=0;
  FSlot:=2;
  FIP:='';
end;

destructor TR7Plc.Destroy;
begin
  FClient.Free;
  inherited Destroy;
end;

procedure TR7Plc.Disconnect;
begin
  if FConnected then
  begin
    FClient.Disconnect;
    FConnected:=false;
  end;
end;

function TR7Plc.Connect: boolean;
begin
  Disconnect;
  Result:=FClient.ConnectTo(FIP,FRack,FSlot)=0;
  FManager.TcpError:=not Result;
end;

function TR7Plc.ReadArea(Area, DBNumber, Start, Amount, WordLen: integer;
  pUsrData: pointer): boolean;
begin
  LastError:=FClient.ReadArea(Area, DBNumber, Start, Amount, WordLen, pUsrData);
  Result:=FLastError=0;
end;

function TR7Plc.GetPlcStatus(Var Status: integer): boolean;
begin
  LastError:=FClient.GetPlcStatus(Status);
  Result:=FLastError=0;
end;

function TR7Plc.GetPlcDateTime(var DateTime: TDateTime): boolean;
begin
  LastError:=FClient.GetPlcDateTime(DateTime);
  Result:=FLastError=0;
end;

function TR7Plc.GetProtection(pUsrData: PS7Protection): boolean;
begin
  LastError:=FClient.GetProtection(pUsrData);
  Result:=FLastError=0;
end;

function TR7Plc.ReadMultiVars(Items: PS7DataItems; ItemsCount: integer): boolean;
begin
  LastError:=FClient.ReadMultiVars(Items, ItemsCount);
  Result:=FLastError=0;
end;

function TR7Plc.Init(Node: TDOMNode): boolean;
Var
  Cpu : integer;
begin
  if Assigned(Node) then
  begin
    Result:=true;
    Cpu:=StrToIntDef(DOMGetProperty(Node,'cpu'),300);
    FFullProtocol:=(Cpu=300) or (Cpu=400);

    FIP:=DOMGetProperty(Node,'ip');
    if FIP<>'' then
    begin
      FRack:=StrToIntDef(DOMGetProperty(Node,'rack'),0);
      FSlot:=StrToIntDef(DOMGetProperty(Node,'slot'),2);
    end
    else
      Result:=false;
  end
  else
    Result:=false;
end;

{ TR7HmiVarWordPage }

procedure TR7HmiVarWordPage.ShowPageInfo;
begin
  case FKind of
    swkTimers   : begin
      FDisplay.Line[1]:=CenterText('PLC VARS');
      FDisplay.Line[2]:=CenterText('TIMERS');
    end;
    swkCounters : begin
      FDisplay.Line[1]:=CenterText('PLC VARS');
      FDisplay.Line[2]:=CenterText('COUNTERS');
    end;
  end;
end;

procedure TR7HmiVarWordPage.ReadTags;
begin
  if FKind=swkTimers then
    Error:=not FPlc.ReadArea(S7AreaTM,0,Index,2,S7WLTimer,@Data)
  else
    Error:=not FPlc.ReadArea(S7AreaCT,0,Index,2,S7WLCounter,@Data);

  Data[0]:=SwapWord(Data[0]);
  Data[1]:=SwapWord(Data[1]);
end;

procedure TR7HmiVarWordPage.ShowTags;
begin
  if not Error then
  begin
    if FKind=swkTimers then
    begin
      FDisplay.Line[1]:=Prefix+JustifyLeft(IntToStr(Index),3,#32)+Blanks(5)+TimValue(Data[0]);
      FDisplay.Line[2]:=Prefix+JustifyLeft(IntToStr(Index+1),3,#32)+Blanks(5)+TimValue(Data[1]);
    end
    else begin
      FDisplay.Line[1]:=Prefix+JustifyLeft(IntToStr(Index),3,#32)+Blanks(8)+IntToHex(Data[0],3);
      FDisplay.Line[2]:=Prefix+JustifyLeft(IntToStr(Index+1),3,#32)+Blanks(8)+IntToHex(Data[1],3);
    end;
  end
  else begin
    FDisplay.Line[1]:=Blanks;
    FDisplay.Line[2]:=TagReadError;
  end;
end;

constructor TR7HmiVarWordPage.Create(Manager: TR7HmiManager; Kind : THmiVarWordKind);
begin
  inherited Create(Manager);
  FKind :=Kind;
  case FKind of
    swkTimers : begin
      Prefix:='T ';
      Area:=S7AreaTM;
      WordLen:=S7WLTimer;
    end;
    swkCounters : begin
      Prefix:='Z ';
      Area:=S7AreaCT;
      WordLen:=S7WLCounter;
    end;
  end;
  Index :=0;
  MinIdx:=0;
  MaxIdx:=254;
end;

{ TR7HmiPageFamily }

function TR7HmiPageFamily.GetPage(index : integer): TR7HmiPage;
begin
  Result:=TR7HmiPage(Items[index]);
end;

constructor TR7HmiPageFamily.Create;
begin
  inherited Create;
  ActivePage:=nil;
end;

procedure TR7HmiPageFamily.Clear;
Var
  c : integer;
begin
  for c:=0 to count-1 do
    Pages[c].Free;
  inherited Clear;
end;

{ TR7HmiThread }

constructor TR7HmiThread.Create(Manager : TR7HmiManager);
begin
  inherited Create(true);
  FreeOnTerminate:=false;
  FManager:=Manager;
end;

procedure TR7HmiThread.Execute;
begin
  while not Terminated do
    FManager.Refresh;
end;

{ TR7HmiManager }

function TR7HmiManager.GetActivePage: TR7HmiPage;
begin
  Result:=Pages[ActiveFamily].ActivePage;
end;

function TR7HmiManager.CheckForNewProgram : boolean;
begin
  Result:=false;
  if FileExists(ProgFileName) then
    Result:=FileAge(ProgFileName)<>LastFileAge;
end;

procedure TR7HmiManager.Welcome;
begin
  FDisplay.Line[1]:=CenterText('Raspy7-HMI');
  FDisplay.Line[2]:=CenterText('1.0.0');
  sleep(WelcomeTime);
end;

function TR7HmiManager.LoadProgram: boolean;

  function LoadXML : boolean;
  begin
    Result:=true;
    try
      ReadXMLFile(XMLDoc, ProgFileName);
    except
      Result:=false;
    end;
  end;

  procedure XMLProgramError;
  begin
    FDisplay.Line[1]:=CenterText('PROGRAM ERROR!');
    FDisplay.Line[2]:=CenterText('  XML FORMAT  ');
  end;

  procedure ProgramNotFound;
  begin
    FDisplay.Line[1]:=CenterText('PROGRAM');
    FDisplay.Line[2]:=CenterText('NOT FOUND');
  end;

  procedure ParseProgramError;
  begin
    FDisplay.Line[1]:=CenterText('PROGRAM ERROR!');
    if ProgramError.Group=-1 then
      FDisplay.Line[2]:=CenterText('PLC CONFIG')
    else
      FDisplay.Line[2]:=CenterText('G:'+IntToStr(ProgramError.Group)+' L:'+IntToStr(ProgramError.Level)+' T:'+IntToStr(ProgramError.TagNo));
  end;

begin
  fillchar(ProgramError,SizeOf(ProgramError),#0);
  Result:=false;
  if FileExists(ProgFileName) then
  begin
    LastFileAge:=FileAge(ProgFileName);
    if LoadXML then
    begin
      Result:=Build;
      if not Result then
        ParseProgramError;
    end
    else
      XMLProgramError;
    XMLDoc.Free;
  end
  else
    ProgramNotFound;

  ProgramValid:=Result;
end;

procedure TR7HmiManager.CreateSysPages;
begin
  FSysPages.Add(TR7HmiSysPage.Create(Self));
  FSysPages.ActivePage:=FSysPages[0];
end;

procedure TR7HmiManager.CreateVarPages;
Var
  c : integer;
begin
  // Digital Inputs
  FVarPages.Add(TR7HmiVarBitsPage.Create(Self,sbkDigitalInputs));
  // Digital Outputs
  FVarPages.Add(TR7HmiVarBitsPage.Create(Self,sbkDigitalOutputs));
  // Merkers
  FVarPages.Add(TR7HmiVarBitsPage.Create(Self,sbkMerkers));
  // Timers
  FVarPages.Add(TR7HmiVarWordPage.Create(Self,swkTimers));
  // Counters
  FVarPages.Add(TR7HmiVarWordPage.Create(Self,swkCounters));
  // Link Left<->Right
  for c:=0 to FVarPages.Count-1 do
  begin
    if c<FVarPages.Count-1 then
      FVarPages[c].PageRight:=FVarPages[c+1];
    if c>0 then
      FVarPages[c].PageLeft:=FVarPages[c-1];
  end;
  FVarPages.ActivePage:=FVarPages[0];
end;

procedure TR7HmiManager.CreateFixedPages;
begin
  CreateSysPages;
  CreateVarPages;
end;

function TR7HmiManager.Build: boolean;
Var
  PlcNode   : TDOMNode;
  GroupNode : TDOMNode;
  TextNode  : TDOMNode;
  UpPage    : TR7HmiPage;
begin
  PlcNode:=XMLDoc.DocumentElement.FirstChild;
  Result:=FPLC.Init(PlcNode);
  if not Result then
  begin
    ProgramError.Group:=-1; // plc group
    exit;
  end;

  if not FPLC.FullProtocol then
    FVarPages[2].PageRight:=nil;

  ProgramError.Group:=-2; // Text node
  TextNode:=PlcNode.NextSibling;
  if Assigned(TextNode) and (TextNode.NodeName='text') then
  begin
    Result:=InitTextList(TextNode);
    if not Result then
      exit;
  end
  else
    exit;

  UpPage:=nil;
  ProgramError.Group:=0;
  GroupNode:=TextNode.NextSibling;
  while Result and Assigned(GroupNode) do
  begin
    inc(GroupCount);
    Result:=NextGroup(GroupNode, GroupCount, UpPage);
    if not Result then
      ProgramError.Group:=GroupCount
    else
      GroupNode:=GroupNode.NextSibling;
  end;

  if Result then
  begin
    if (GroupList.Count>0) then
    begin
      LinkGroups;
      FUsrPages.ActivePage:=TR7HmiUsrPage(GroupList[0]);
      FUsrPages.ActivePage.Active:=true;
    end
    else
      FUsrPages.ActivePage:=NullPage
  end;

end;

function TR7HmiManager.NextGroup(Node: TDOMNode; GroupIndex : integer;
  var UpPage : TR7HmiPage): boolean;
Var
  PageNode : TDOMNode;
  GroupName: string;
  Level    : integer;
  LeftPage : TR7HMiPage;
  FirstPage: TR7HMiPage;
begin
  GroupName:=DOMGetProperty(Node,'name');
  if GroupName='' then
    GroupName:='GROUP : '+IntToStr(GroupIndex);

  Level:=0;
  Result:=true;
  LeftPage:=nil;
  PageNode:=Node.FirstChild;
  while Result and Assigned(PageNode) do
  begin
    inc(Level);
    Result:=NextPage(PageNode, Level, GroupName, UpPage, LeftPage);

    if not Result then
      ProgramError.Level:=Level;

    if Result and (Level=1) then
    begin
      GroupList.Add(LeftPage);
      FirstPage:=LeftPage;
    end;
    PageNode:=PageNode.NextSibling;
  end;

  if Result then
    UpPage:=FirstPage;
end;

function TR7HmiManager.NextPage(Node: TDOMNode; Level: integer;
  GroupName : string; var UpPage, LeftPage: TR7HmiPage): boolean;
Var
  Page : TR7HmiUsrPage;
begin
  Page:=TR7HmiUsrPage.Create(Self);
  Page.GroupName:=GroupName;
  UsrPages.Add(Page);
  Page.PageLeft:=LeftPage;
  if Assigned(LeftPage) then
    LeftPage.PageRight:=Page;

  Page.PageUp:=UpPage;
  LeftPage:=Page;

  Result:=Page.Init(Node, Level);
end;

function TR7HmiManager.InitTextList(Node : TDOMNode): boolean;
Var
  MsgNode : TDOMNode;
  PropNode: TDOMNode;
  txt : AnsiString;
  sidx: string;
  idx,c : integer;
begin
  Result:=true;
  MsgNode:=Node.FirstChild;
  while Assigned(MsgNode) and Result do
  begin
    Txt:=DOMGetProperty(MsgNode,'val');
    if DOMExistsProperty(MsgNode,'idx') then
    begin
      PropNode:=MsgNode.Attributes.GetNamedItem('idx');
      sidx:=PropNode.NodeValue;
      Val(sidx,idx,c);
      Result:=c=0;
      if Result then
      begin
        FText.AddObject(txt,TObject(idx));
        MsgNode:=MsgNode.NextSibling;
      end;
    end;
  end;
end;

procedure TR7HmiManager.LoadGlobals;
Var
  ini : TMemIniFile;
begin
  if FileExists(IniFileName) then
  begin
    ini := TMemIniFile.Create(IniFileName);
    try
      I2cBusNumber    :=ini.ReadInteger('BOARD','I2cBusNumber',I2cBusNumber);
      MCP23017_Address:=ini.ReadInteger('BOARD','MCP23017_Address',MCP23017_Address);
      NewPrgTimeout   :=ini.ReadInteger('PARAMS','NewPrgTimeout',NewPrgTimeout);
      HmiRate         :=ini.ReadInteger('PARAMS','HmiRate',HmiRate);
      if HmiRate<50 then
        HmiRate:=50;
      PageInfoTime    :=ini.ReadInteger('PARAMS','PageInfoTime',PageInfoTime);
      MenuInfoTime    :=ini.ReadInteger('PARAMS','MenuInfoTime',MenuInfoTime);
      FastKeyTime     :=ini.ReadInteger('PARAMS','FastKeyTime',FastKeyTime);
      WelcomeTime     :=ini.ReadInteger('PARAMS','WelcomeTime',WelcomeTime);
    finally
      ini.Free;
    end;
  end;
end;

procedure TR7HmiManager.LinkGroups;
Var
  c : integer;
  RightPage : TR7HmiPage;
begin
  RightPage:=nil;
  if GroupList.Count>2 then
  begin
    for c:=0 to GroupList.Count-2 do
    begin
      GroupList[c].PageDn:=GroupList[c+1];
      RightPage:=GroupList[c].PageRight;
      while Assigned(RightPage) do
      begin
        RightPage.PageDn:=GroupList[c+1];
        RightPage:=RightPage.PageRight;
      end;
    end;
  end;
end;

procedure TR7HmiManager.Clear;
begin
  GroupCount:=0;
  GroupList.Clear;
  FUsrPages.Clear;
  FText.Clear;
end;

function TR7HmiManager.Connect: boolean;
Const
  ConnChars : array[0..5] of AnsiChar = ('|','/','-','|','/','-');
begin
  inc(ConnCnt);
  if ConnCnt>5 then
    ConnCnt:=0;
  FDisplay.Line[1]:='CONNECTING TO  '+ConnChars[ConnCnt];
  FDisplay.Line[2]:=CenterText(FPlc.IP);
  Result:=FPlc.Connect;
end;

function TR7HmiManager.CreateUsrPages: boolean;
begin
  result:=false;
end;

procedure TR7HmiManager.Stop;
begin
  if not FMainThread.Suspended then
  begin
    FMainThread.Terminate;
    FMainThread.WaitFor;
  end;
end;

constructor TR7HmiManager.Create;
begin
  inherited Create;
  FPlc:=TR7Plc.Create(Self);
  FMainThread:=TR7HmiThread.Create(Self);
  Elapse(PrgCounter);
  ActiveFamily := fkUser;
  TcpError:=false;
  GroupCount:=0;
  LastFileAge:=0;
  FVarPages :=TR7HmiPageFamily.Create;
  FSysPages :=TR7HmiPageFamily.Create;
  FUsrPages :=TR7HmiPageFamily.Create;
  GroupList :=TR7HmiGroupList.Create;
  FDisplay  :=TR7HmiDisplay.Create;
  FText     :=TStringList.Create;
  Pages[fkUser]:=FUsrPages;
  Pages[fkVar] :=FVarPages;
  Pages[fkSys] :=FSysPages;
  NullPage := TR7HmiNullPage.Create(Self);
  FKeyManager:=TR7HmiKeyManager.Create(Self);
end;

function TR7HmiManager.Init: boolean;
begin
  LoadGlobals;
  Result:=FDisplay.Init(I2cBusNumber, MCP23017_Address);
  if Result then
  begin
    Welcome;
    CreateFixedPages;
    if LoadProgram then
      Connect;
    FMainThread.Start;
  end;
end;

procedure TR7HmiManager.ReInit;
begin
  FPlc.Disconnect;
  FKeyManager.Reset;
  Clear;
  if LoadProgram then
    Connect;
end;

destructor TR7HmiManager.Destroy;
begin
  Stop;
  FMainThread.Free;
  FKeyManager.Free;
  FDisplay.Free;
  FVarPages.Free;
  FSysPages.Free;
  FUsrPages.Free;
  GroupList.Free;
  NullPage.Free;
  FPlc.Free;
  FText.Free;
  inherited Destroy;
end;

procedure TR7HmiManager.KeyAction(Action: TKeyAction);
begin
  if Assigned(ActivePage) then
    ActivePage.SwitchPage(TNextPage(Action));
end;

procedure TR7HmiManager.Refresh;
begin
  inc(HmiCnt);
  if ProgramValid then
  begin
    if not TcpError then
    begin
      if FKeyManager.Refresh then
      begin
        if Assigned(ActivePage) then
          ActivePage.Refresh;
        sleep(HmiRate);
      end
      else
        sleep(10); // Keys refresh rate
    end
    else
      if not Connect then
        Sleep(1000);
  end;

  // Check if there is an uploaded program
  if DeltaTime(PrgCounter)>NewPrgTimeout then
  begin
    Elapse(PrgCounter);
    if CheckForNewProgram then
      ReInit;
  end;
end;

function TR7HmiManager.GetIndexedText(index: integer): AnsiString;
Var
  c : integer;
begin
  for c:=0 to FText.Count-1 do
    if index=integer(FText.Objects[c]) then
    begin
      Result:=FText[c];
      exit;
    end;
  Result:='';
end;

{ TR7HmiSysPage }

procedure TR7HmiSysPage.View0;
begin
  FDisplay.Line[1]:=CenterText('PLC IP ADDRESS');
  FDisplay.Line[2]:=CenterText(FPlc.IP);
end;

procedure TR7HmiSysPage.View1;
Var
  Status : integer;
begin
  Error:=not FPlc.GetPlcStatus(Status);
  FDisplay.Line[1]:=CenterText('CPU STATUS');
  if not Error then
  begin
    case Status of
      S7CpuStatusRun  : FDisplay.Line[2]:=CenterText('RUN');
      S7CpuStatusStop : FDisplay.Line[2]:=CenterText('STOP');
      else
        FDisplay.Line[2]:=CenterText('UNKNOWN');
    end;
  end
  else
    FDisplay.Line[2]:=TagReadError;
end;

procedure TR7HmiSysPage.View2;
Var
  DT : TDateTime;
begin
  Error:=not FPlc.GetPlcDateTime(DT);
  FDisplay.Line[1]:=CenterText('CPU DATE');
  if not Error then
    FDisplay.Line[2]:=CenterText(DateToStr(DT))
  else
    FDisplay.Line[2]:=TagReadError;
end;

procedure TR7HmiSysPage.View3;
Var
  DT : TDateTime;
begin
  Error:=not FPlc.GetPlcDateTime(DT);
  FDisplay.Line[1]:=CenterText('CPU TIME');
  if not Error then
    FDisplay.Line[2]:=CenterText(TimeToStr(DT))
  else
    FDisplay.Line[2]:=TagReadError;
end;

procedure TR7HmiSysPage.View4;
Const
  Selector : array[0..4] of AnsiString = (
   'UNKNOWN','RUN','RUN-P','STOP','MRES'
  );
Var
  Info : TS7Protection;
begin
  Error:=not FPlc.GetProtection(@Info);
  FDisplay.Line[1]:=CenterText('CPU SELECTOR');
  if not Error then
  begin
    if Info.bart_sch>4 then
      Info.bart_sch:=0;
    FDisplay.Line[2]:=CenterText(Selector[Info.bart_sch]);
  end
  else
    FDisplay.Line[2]:=TagReadError;
end;

procedure TR7HmiSysPage.View5;
Const
  ProLevel : array[0..3] of AnsiString = (
   'UNKNOWN','GRANT','READ ONLY','READ/WRITE'
  );
Var
  Info : TS7Protection;
begin
  Error:=not FPlc.GetProtection(@Info);
  FDisplay.Line[1]:=CenterText('CPU PROTECTION');
  if not Error then
  begin
    if Info.sch_rel>3 then
      Info.sch_rel:=0;
    FDisplay.Line[2]:=CenterText(ProLevel[Info.sch_rel]);
  end
  else
    FDisplay.Line[2]:=TagReadError;
end;

procedure TR7HmiSysPage.ReadTags;
begin
  // SysPages perform read and show in the same procedure
  case View of
    0 : View0;
    1 : View1;
    2 : View2;
    3 : View3;
    4 : View4;
    5 : View5;
  end;
end;

procedure TR7HmiSysPage.ShowTags;
begin
end;

procedure TR7HmiSysPage.ShowPageInfo;
begin
  FDisplay.Line[1]:=CenterText('SYSTEM');
  FDisplay.Line[2]:=Blanks;
end;

constructor TR7HmiSysPage.Create(Manager: TR7HmiManager);
begin
  inherited Create(Manager);
  Family:=Manager.SysPages;
  View:=0;
end;

procedure TR7HmiSysPage.SwitchPage(Where: TNextPage);
Var
  Max : integer;
begin
  if FPlc.FullProtocol then
    Max:=5
  else
    Max:=0;
  case Where of
    npLeft:
      if View>0 then
        Dec(View);
    npRight:
      if View<Max then
        Inc(View);
  end;
end;

{ TR7HmiVarPage }

constructor TR7HmiVarPage.Create(Manager: TR7HmiManager);
begin
  inherited Create(Manager);
  Family:=Manager.VarPages;
  Index:=0;
end;

procedure TR7HmiVarPage.SwitchPage(Where: TNextPage);
Var
  Changed : boolean;
begin
  Changed:=false;
  case Where of
    npUp:
      if Index>MinIdx then
        Dec(Index);
    npDn:
      if Index<MaxIdx then
        Inc(Index);
    npLeft:
      if PageLeft<>nil then
      begin
        PageLeft.Active:=true;
        Changed:=true;
      end;
    npRight:
      if PageRight<>nil then
      begin
        PageRight.Active:=true;
        Changed:=true;
      end;
  end;
  if Changed then
    Active:=false;
end;

{ TR7HmiKeyManager }

procedure TR7HmiKeyManager.ShowMenu;
begin
  FDisplay.Line[1]:='1 : USR  2 : VAR';
  FDisplay.Line[2]:='3 : SYS         ';
end;

constructor TR7HmiKeyManager.Create(Manager: TR7HmiManager);
begin
  inherited Create;
  FManager:=Manager;
  FDisplay:=Manager.Display;;
end;

procedure TR7HmiKeyManager.Reset;
begin
  fillchar(K,SizeOf(TKeyStates),#0);
end;

function TR7HmiKeyManager.Refresh : boolean;
Var
  KUp, KDn, KLf, KRh, KEn : boolean;
  AUp, ADn, ALf, ARh, AEn : boolean;
  Keys : byte;
begin
  // Get key values
  Keys:=FDisplay.GetKeys;
  KEn:=(Keys and $01)<>0;


  KDn:=(Keys and $04)<>0;
  KUp:=(Keys and $08)<>0;

  KRh:=(Keys and $02)<>0;
  KLf:=(Keys and $10)<>0;

  // Mutually exclusive Key values to avoid multiple key pressing
  AUp :=KUp and not KDn and not KLf and not KRh and not KEn;
  ADn :=not KUp and KDn and not KLf and not KRh and not KEn;
  ALf :=not KUp and not KDn and KLf and not KRh and not KEn;
  ARh :=not KUp and not KDn and not KLf and KRh and not KEn;
  AEn :=not KUp and not KDn and not KLf and not KRh and KEn;

  // Actions One shoot
  with K do
  begin
    OsUp:=AUp and not StUp;
    StUp:=AUp;
    OsDn:=ADn and not StDn;
    StDn:=ADn;
    OsLf:=ALf and not StLf;
    StLf:=ALf;
    OsRh:=ARh and not StRh;
    StRh:=ARh;
    OsEn:=AEn and not StEn;
    StEn:=AEn;

    // Fast UP detection
    if OsUp or not AUp then
      Elapse(UpCounter);

    if AUp and (DeltaTime(UpCounter)>FastKeyTime) then
      FManager.KeyAction(kaUp);

    // Fast DN detection
    if OsDn or not ADn then
      Elapse(DnCounter);

    if ADn and (DeltaTime(DnCounter)>FastKeyTime) then
      FManager.KeyAction(kaDn);

    // Move
    if OsUp and not Menu then FManager.KeyAction(kaUp);
    if OsDn and not Menu then FManager.KeyAction(kaDn);
    if OsLf and not Menu then FManager.KeyAction(kaLeft);
    if OsRh and not Menu then FManager.KeyAction(kaRight);

    // Menu --------------------------------------------------------------------

    // Drop menu if Enter re-repressed
    if OsEn and Menu then
    begin
      Menu:=false;
      OsEn:=false;
    end;

    // Show menu on Enter one shoot
    if OsEn and not Menu then
    begin
      Menu:=true;
      ShowMenu;
      Elapse(MnCounter);
    end;

    // Select menu item if menu active and a selection key pressed
    if Menu and OsLf then
    begin
      FManager.ActiveFamily:=fkUser;
      Menu:=false;
    end;
    if Menu and OsRh then
    begin
      FManager.ActiveFamily:=fkVar;
      Menu:=false;
    end;
    if Menu and OsUp then
    begin
      FManager.ActiveFamily:=fkSys;
      Menu:=false;
    end;

    // Drop menu on Timeout without selection
    if Menu and (DeltaTime(MnCounter)>MenuInfoTime) then
      Menu:=false;

  end;

  // Result=false ==> Menu is active and no page must be refreshed
  result:=not K.menu;
end;

procedure TR7HmiVarBitsPage.ShowPageInfo;
begin
  case FKind of
    sbkDigitalInputs  : begin
      FDisplay.Line[1]:=CenterText('PLC VARS');
      FDisplay.Line[2]:=CenterText('DIGITAL INPUTS');
    end;
    sbkDigitalOutputs : begin
      FDisplay.Line[1]:=CenterText('PLC VARS');
      FDisplay.Line[2]:=CenterText('DIGITAL OUTPUTS');
    end;
    sbkMerkers        : begin
      FDisplay.Line[1]:=CenterText('PLC VARS');
      FDisplay.Line[2]:=CenterText('MERKERS');
    end;
  end;
end;

procedure TR7HmiVarBitsPage.ReadTags;
begin
  Error:=not FPlc.ReadArea(Area,0,Index,1,WordLen,@Data);
end;

procedure TR7HmiVarBitsPage.ShowTags;
begin
  if not Error then
  begin
    FDisplay.Line[1]:=Prefix+JustifyLeft(IntToStr(Index),5,#32)+' 76543210';
    FDisplay.Line[2]:=JustifyRight(IntToStr(Data),3,'0')+' $'+IntToHex(Data,2)+' '+ByteToBin(Data);
  end
  else begin
    FDisplay.Line[1]:=Blanks;
    FDisplay.Line[2]:=TagReadError;
  end;
end;

constructor TR7HmiVarBitsPage.Create(Manager : TR7HmiManager; Kind : THmiVarBitKind);
begin
  inherited Create(Manager);
  FKind:=Kind;
  case FKind of
    sbkDigitalInputs : begin
      Prefix:='EB';
      Area:=S7AreaPE;
    end;
    sbkDigitalOutputs : begin
      Prefix:='AB';
      Area:=S7AreaPA;
    end;
    sbkMerkers : begin
      Prefix:='MB';
      Area:=S7AreaMK;
    end;
  end;
  WordLen:=S7WLByte;
  Index :=0;
  MinIdx:=0;
  MaxIdx:=2047;
end;

{ TR7HmiPage }

procedure TR7HmiPage.SetFActive(AValue: boolean);
begin
  if FActive<>AValue then
  begin
    FActive  :=AValue;
    PageInfo :=FActive;
    if FActive then
      Family.ActivePage:=self;
  end;
end;

procedure TR7HmiPage.ShowPageInfo;
begin
end;

procedure TR7HmiPage.SetFPageInfo(AValue: boolean);
begin
  if FPageInfo<>AValue then
  begin
    FPageInfo:=AValue;
    if FPageInfo then
      Elapse(PICounter);
  end;
end;

procedure TR7HmiPage.ReadTags;
begin
end;

procedure TR7HmiPage.ShowTags;
begin
  FDisplay.Line[1]:=blanks;
  FDisplay.Line[2]:=blanks;
end;

constructor TR7HmiPage.Create(Manager: TR7HmiManager);
begin
  inherited Create;
  FManager  :=Manager;
  FDisplay  :=FManager.Display;
  FPlc      :=FManager.PLC;
  PageUp    :=nil;
  PageDn    :=nil;
  PageLeft  :=nil;
  PageRight :=nil;
end;

procedure TR7HmiPage.Refresh;
begin
  if PageInfo and (DeltaTime(PICounter)>PageInfoTime) then
    PageInfo:=false;

  if not PageInfo then
  begin
    ReadTags;
    ShowTags;
  end
  else
    ShowPageInfo;
end;

procedure TR7HmiPage.SwitchPage(Where: TNextPage);
Var
  Changed : boolean;
begin
  Changed:=false;
  case Where of
    npUp:
      if PageUp<>nil then
      begin
        PageUp.Active:=true;
        Changed:=true;
      end;
    npDn:
      if PageDn<>nil then
      begin
        PageDn.Active:=true;
        Changed:=true;
      end;
    npLeft:
      if PageLeft<>nil then
      begin
        PageLeft.Active:=true;
        Changed:=true;
      end;
    npRight:
      if PageRight<>nil then
      begin
        PageRight.Active:=true;
        Changed:=true;
      end;
  end;
  if Changed then
    Active:=false;
end;

{ TR7HmiUsrPage }

function TR7HmiUsrPage.TagTypeOf(TypeName: string): TR7TagType;
begin
  for Result:=ttUnknown to ttConst do
    if UpperCase(TypeName)=TypeNames[Result] then
      exit;
  Result:=ttUnknown;
end;

procedure TR7HmiUsrPage.ShowPageInfo;
begin
  FDisplay[1]:=CenterText(GroupName);
  FDisplay[2]:=CenterText(LevelName);
end;

procedure TR7HmiUsrPage.ReadTags;
begin
  if ActiveTags>0 then
    Error:=not FPLC.ReadMultiVars(@DataItems,ActiveTags)
  else
    Error:=false;
end;

procedure TR7HmiUsrPage.ShowTags;
begin
   if not Error then
   begin
     Tags.Show;
     FDisplay.Line[1]:=Buffer[1];
     FDisplay.Line[2]:=Buffer[2];
   end
   else begin
     FDisplay.Line[1]:=Blanks;
     FDisplay.Line[2]:=TagReadError;
   end;
end;

constructor TR7HmiUsrPage.Create(Manager: TR7HmiManager);
begin
  inherited Create(Manager);
  Tags:=TR7HmiTags.Create(Self);

  Family:=FManager.UsrPages;
end;

destructor TR7HmiUsrPage.Destroy;
begin
  Tags.Free;
  inherited Destroy;
end;

function TR7HmiUsrPage.Init(Node: TDOMNode; Level : integer): boolean;
Var
  TagCount: integer;
  TagNode : TDOMNode;
  TagTypeName : string;
  TagType : TR7TagType;
  Tag : TR7HmiTag;
begin
  LevelName:=DOMGetProperty(Node,'name');
  if LevelName='' then
    LevelName:='LEVEL : '+IntToStr(Level);

  Result:=true;
  TagCount:=0;

  TagNode:=Node.FirstChild;
  while Assigned(TagNode) and Result do
  begin
    inc(TagCount);
    TagTypeName:=Trim(DOMGetProperty(TagNode,'type'));
    if TagTypeName<>'' then
    begin
      TagType:=TagTypeOf(TagTypeName);
      if TagType<>ttUnknown then
      begin
        Tag:=nil;
        case TagType of
          ttBit      : Tag:=TR7HmiBitTag.Create(Self);
          ttInt      : Tag:=TR7HmiIntTag.Create(Self,false);
          ttUInt     : Tag:=TR7HmiIntTag.Create(Self,true);
          ttReal     : Tag:=TR7HmiRealTag.Create(Self);
          ttChars    : Tag:=TR7HmiCharsTag.Create(Self);
          ttTxtIndex : Tag:=TR7HmiTextIndexTag.Create(Self);
          ttConst    : Tag:=TR7HmiCstTag.Create(Self);
        end;
        if Assigned(Tag) then
          Result:=Tag.Init(TagNode)
        else
          Result:=false;
      end
      else
        Result:=false;
    end
    else
      Result:=false;

    if Result then
    begin
      TagNode:=TagNode.NextSibling;
      Tags.Add(Tag);
      if Tags.Count=MaxVars then
        TagNode:=nil;
    end
    else
      FManager.ProgramError.TagNo:=TagCount;
  end;
end;

{ TR7HmiGroupList }

function TR7HmiGroupList.GetGroup(index : integer): TR7HmiPage;
begin
  Result:=TR7HmiPage(items[index])
end;

end.

